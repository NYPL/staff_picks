# Staff Picks Data Import

This repo contains both the data for
[Staff Picks](http://www.nypl.org/browse/recommendations/staff-picks/)
lists as well as a couple of tools for converting from TSV to JSON

## Workflow

### Export the Spreadsheet to TSV

First a Google Sheet comes from Reader's Services. This needs to be
exported as a tab-delimited file and added to the `source`
directory. Usually the format of the spreadsheet isn't quite right and
it needs some fixing. Depending on the issues you may want to do that
in the spreadsheet before exporting it or in the tab delimited file
after. The one requirement is that columns with the following headers
should be present:

    "age",
    "author",
    "catalog_uri",
    "ebook_uri",
    "image_uri",
    "location",
    "name",
    "tags",
    "text",
    "title"

The order doesn't matter, but it is case-sensitive. Other than that,
sometimes tabs creep into the cells etc.

If the spreadsheets are for Children's 100 or YA's 100. No need to add missing
tags then, but make sure the spelling is correct.

### Check the tags

Run the `check-tags` script over the file. For instance, if the
exported file is named `2016-07-01.tsv`:

    > cd scripts
    > ./check-tags ../source/2016-07-01.tsv

`check-tags` makes sure that all tags in the TSV file exist in the
`tags.yaml` file in `source`. If it reports a tag not found, you will
need to correct it (spelling for instance) or add it to `tags.yaml`

The tags are separated by commas in the cells, and sometimes you need
to add missing or delete stray commas before `check-tags` will work.

### Convert to JSON

This step is simple. Just run `to-json` over the TSV file

    > ./to-json ../source/2016-07-01.tsv

This will create a `2016-07-01.json` file in the repository root.

### Add it to the Manifest

It will probably look something like this:

      - source-file:   2016-07-01.tsv
        json-file  :   2016-07-01.json
        date       :   2016-07-01
        type       :   monthly

Lists are quarterly now, but they used to be monthly, so unless it is
a "Childrens' 100" list, give it that type. The date only needs to be
accurate to the month.

### Commit the changes

Once work is commited to the right branch it is ready to import into
the refinery, `dev`, `qa` and `production` branches going into their
respective Refinery instances. The update can be forced by making the
appropriate request in a browser for the corresponding dev, qa, or production servers, e.g.:

This URL is for updating all lists. With the filter we are able to see the latest monthly list:
https://dev-refinery.nypl.org/api/nypl/ndo/v0.1/staff-picks/staff-pick-lists?filter[list-type]=monthly&fields[staff-pick-tag]=tag&fields[staff-pick-age]=age&fields[staff-pick-item]=title,author,catalog-slug,image-slug,tags,ebook-uri&page[size]=1&include=previous-list,next-list,picks.item.tags,picks.age

For annual lists, update the `filter[list-type]` to `ya100` or `c100`.

https://dev-refinery.nypl.org/api/nypl/ndo/v0.1/staff-picks/staff-pick-lists/monthly-2017-06-01?fields[staff-pick-tag]=tag&fields[staff-pick-age]=age&fields[staff-pick-item]=title,author,catalog-slug,image-slug,tags,ebook-uri&include=previous-list,next-list,picks.item.tags,picks.age

and refreshing _twice_;
